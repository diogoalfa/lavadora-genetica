/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lavadora_genetica;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author macbookpro
 */
public class Poblacion {
    
    List<Lavadora> poblacion;
    Lavadora lavadoraAlfa;

    //Crear poblacion 
    public Poblacion(int largoPoblacion,Random randDado) {
        poblacion=new ArrayList<>();
        Lavadora hija1=new Lavadora(1, randDado);
        Lavadora hija2=new Lavadora(1, randDado);
        List<Lavadora> listLavadora=new ArrayList<>();
        int fitness=-999999999;
        if (poblacion.isEmpty()) {
            poblacion=new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                if(randDado.nextInt(100)<80){
                    listLavadora=hija1.cruzamiento(hija2, 1);
                    hija1=listLavadora.get(0);
                    hija2=listLavadora.get(1);
                }
                if(randDado.nextInt(100)<30){
                    hija1.mutacion(randDado);
                    hija2.mutacion(randDado);
                }
                poblacion.add(hija1);
                poblacion.add(hija2);
            }
        }
        if(!poblacion.isEmpty()){
            for (int i = 0; i < largoPoblacion; i++) {
                if(randDado.nextInt(100)<80){
                    listLavadora=hija1.cruzamiento(hija2, 1);
                    hija1=listLavadora.get(0);
                    hija2=listLavadora.get(1);
                }
                if(randDado.nextInt(100)<30){
                    hija1.mutacion(randDado);
                    hija2.mutacion(randDado);
                }
                poblacion.add(hija1);
                poblacion.add(hija2);
            }
            
        }
    }
    
    public Lavadora getlavadoraAlfa(){
        Random randDado=new Random();
        lavadoraAlfa=this.poblacion.get(0);
        for (int i = 0; i < poblacion.size(); i++) {
            if(lavadoraAlfa.fitness()<poblacion.get(i).fitness()){
                lavadoraAlfa=poblacion.get(i);
            }
        }
        return lavadoraAlfa;
    }
    

    public List<Lavadora> getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(List<Lavadora> poblacion) {
        this.poblacion = poblacion;
    }

    @Override
    public String toString() {
        System.out.println("Poblacion : ");
        for (int i = 0; i < poblacion.size(); i++) {
            System.out.println("{ Nº"+i+":"+poblacion.get(i)+"|fitness :"+poblacion.get(i).fitness()+"}");
        }
        this.lavadoraAlfa=this.getlavadoraAlfa();
        return "{ lavadoraAlfa=" + this.lavadoraAlfa +"| Fitness : "+this.lavadoraAlfa.fitness()+ '}';
    }
    
    
}
