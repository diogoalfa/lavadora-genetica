/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lavadora_genetica;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author macbookpro
 */
public class Lavadora {
    private int[] adn;
    private int prioridad;
    private int largoADN=6;

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public Lavadora(int[] adn,int prioridad) {
        this.adn = adn;
        this.prioridad=prioridad;
    }
    
    public Lavadora(int prioridad,Random rand){
       this.adn=new int[6];
       this.prioridad=prioridad;
       for (int i = 0; i < 6; i++) {
            adn[i]=rand.nextInt(50);
       }
      
    }
    
    public int[] getAdn() {
        return adn;
    }

    public void setAdn(int[] adn) {
        this.adn = adn;
    }
    
    public List<Lavadora> cruzamiento(Lavadora lavadora2,int dado){
        List<Lavadora> listLavadora=new ArrayList();
        //System.out.println("DADO : "+dado);
        int[] adn1 = new int[largoADN];
        int[] adn2 = new int[largoADN];

        //LLenar los dos string con con las primeros caracteres de las soluciones habitantes
        for (int i = 0; i < lavadora2.getAdn().length; i++) {

            if (i > dado) {
            } else {
                adn1[i]=lavadora2.getAdn()[i];// lavadora que entra
                adn2[i]=this.getAdn()[i];    //lavadora interna
                //System.out.println("Sol 1 priori :"+habTemp1);
            }
        }
        //   System.out.println("primero digitos :"+habTemp1);
        //   System.out.println("primero digitos :"+habTemp2);
        //--------------------------
        for (int i = 0; i < lavadora2.getAdn().length; i++) {
            //System.out.println("Temp 1 :"+habTemp1);
            //System.out.println(hab2.charAt(i)+"||"+"(-1) :"+habTemp1.indexOf(hab2.charAt(i)));
            if (i > dado) {
                adn1[i] = this.getAdn()[i];
                // System.out.println("Que pasa HAB 1 :"+habTemp1);
            }
            if (i > dado) {
                adn2[i] = lavadora2.getAdn()[i];
            }

        }
        listLavadora.add(new Lavadora(adn1,this.prioridad));
        listLavadora.add(new Lavadora(adn2,lavadora2.getPrioridad()));
        
        return listLavadora;
    }
    
    public void mutacion(Random rand){
        int[] adn1=new int[largoADN];
        
        int dado1=rand.nextInt(largoADN);
        int dado2=rand.nextInt(largoADN);
       
        adn1=this.getAdn();
        int aux1=adn1[dado1];
        int aux2=adn1[dado2];
        
        adn1[dado2]=aux1;
        adn1[dado1]=aux2;
        
        this.setAdn(adn1);
    }
    
    public int fitness(){
        
        int suma=0;
        for (int i = 0; i < adn.length; i++) {
            suma=suma-adn[i];
        }
        return suma;
    }

    @Override
    public String toString() {
        String adnMostrar="";
        for (int i = 0; i < this.getAdn().length; i++) {
            adnMostrar=adnMostrar+"["+adn[i]+"]";
        }
        return "Lavadora{" + "adn=" + adnMostrar + ", prioridad=" + prioridad + '}';
    }
    
    
    
}
